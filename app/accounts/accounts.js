'use strict';

angular.module('myApp.view1', ['ngRoute', 'ngCookies'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/accounts', {
    templateUrl: 'accounts/accounts.html',
    controller: 'accountCtrl'
  });
}])

.controller('accountCtrl', ['$scope','$http','$route', '$cookies', function($scope, $http, $route, $cookies) {
		$scope.transection={};
		$scope.net = {}; 
		$http({
			url:'http://localhost:3000/api/getaccounts',
			method:'GET',
			headers:{'Authorization': $cookies.get('session_id')},
		}).then(function(response){
			$scope.accounts = response.data;
			$scope.net.total = gettotal(response.data);
			console.log($scope.net);
		})

		function gettotal(accountArr){
			return accountArr.reduce((a,b)=>{
				console.log(a.amount);
			})
		}

		$scope.createTransection = function(transData){
			$http({
				url:'http://localhost:3000/api/createtransection',
				method:'POST',
				data:transData,
				headers:{'Authorization': $cookies.get('session_id'), 'Content-Type':'application/json'},
			}).then(function(response){
				$scope.accounts = response.data;
				$route.reload();
			})
		}
	
}]);