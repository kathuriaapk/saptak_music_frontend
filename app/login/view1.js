'use strict';

angular.module('myApp.login', ['ngRoute', 'ngCookies'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/login', {
    templateUrl: 'login/login.html',
    controller: 'loginCtrl'
  });
}])

.controller('loginCtrl', ['$scope','$http','$location', '$cookies', function($scope, $http, $location, $cookies) {
		
	$scope.login = function(username, password){
		var logindata = {
			username:username,
			password:password
		}
		$http({
			method:'POST',
			url:'http://localhost:3000/api/login',
			data:logindata
		}).then(function onsuccess(response){
			$cookies.put('session_id', response.data.token);
			window.location.assign('#!/dashboard');
		})
	}
}]);
// 'use strict';

// angular.module('myApp.view1', ['ngRoute', 'ngCookies'])
// .config(['$routeProvider', function($routeProvider) {
//   $routeProvider.when('/login', {
//     templateUrl: 'view1/login.html',
//     controller: 'loginCtrl'
//   });
// }])



// }]);