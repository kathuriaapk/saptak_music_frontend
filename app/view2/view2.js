'use strict';

angular.module('myApp.view2', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/dashboard', {
    templateUrl: 'view2/dashboard.html',
    controller: 'dashboardCtrl'
  });
}])

.controller('dashboardCtrl', [function() {

}]);