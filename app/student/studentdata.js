angular.module('myApp.studentdata', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/studentdata', {
    templateUrl: 'student/studentdata.html',
    controller: 'stuDataCtrl'
  });
  $routeProvider.when('/studentdetail/:id', {
    templateUrl: 'student/studentdetail.html',
    controller: 'stuDetailCtrl'
  });
}])

.controller('stuDataCtrl', ['$scope','$http','$location','$cookies', function($scope, $http, $location, $cookies) {
		$http({
			url :'http://localhost:3000/api/getallstudents',
			method : 'GET',
			headers:{'Authorization': $cookies.get('session_id')} 
		}).then(function onsuccess(response){
			$scope.studentData = response.data;
		})

}])
.controller('stuDetailCtrl', ['$scope','$http','$route','$cookies','$routeParams', function($scope, $http, $route, $cookies, $routeParams) {
		console.log($routeParams.id);
		$http({
			url :'http://localhost:3000/api/getstudentdetail/'+$routeParams.id,
			method : 'GET',
			headers:{'Authorization': $cookies.get('session_id')} 
		}).then(function onsuccess(response){
			$scope.studentDetail=response.data[0];
			getfeedetail($routeParams.id);
			// /$scope.studentDetail = response.data;
		})

		function getfeedetail(regId){
			$http({
			url :'http://localhost:3000/api/getfeedetail/'+$routeParams.id,
			method : 'GET',
			headers:{'Authorization': $cookies.get('session_id')} 
		}).then(function onsuccess(response){
			$scope.feeData = response.data;

			// /$scope.studentDetail = response.data;
		})	
	}

		$scope.fee = {};

		$scope.addFee = function(regId, feeData){
			feeData.reg_id = regId;
			feeData.type = 'credit';
			console.log(feeData);
			$http({
			url :'http://localhost:3000/api/addFee',
			method:'POST',
			data : feeData,
			headers:{'Authorization': $cookies.get('session_id'), 'Content-Type':'application/json'} 
			}).then(function onsuccess(response){
			console.log(response.data);
			$scope.studentDetail=response.data[0];
			$route.reload();
			// /$scope.studentDetail = response.data;
			})	
		}

}]);